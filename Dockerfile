FROM node:16.15-bullseye

WORKDIR /app

COPY ./package.json ./

RUN yarn install --production
RUN yarn add pg

COPY . .

ENV NODE_ENV production
ENV DB_CLIENT='postgres'

RUN yarn build

EXPOSE 1337

CMD ["npm", "start"]

